﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository,
            IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Обновляет список ролей для сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <param name="employee"></param>
        /// <remarks>Проверяет на существование и дубли выбранные роли</remarks>
        /// <returns></returns>
        private async Task<string> UpdateRoles(EmployeeRequest employeeRequest, Employee employee)
        {
            employee.Roles = new List<Role>();
            foreach (var roleName in employeeRequest.Roles)
            {
                var foundedRole = await _roleRepository.GetByAsync(x =>
                    string.Equals(x.Name, roleName, StringComparison.OrdinalIgnoreCase));

                if (foundedRole != null)
                {
                    if (!employee.Roles.Contains(foundedRole))
                    {
                        employee.Roles.Add(foundedRole);
                    }
                }
                else
                {
                    return $"Role named '{roleName}' not found";
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            var employeeToCreate = _mapper.Map<Employee>(employeeRequest);

            var result = await UpdateRoles(employeeRequest, employeeToCreate);
            if (!string.IsNullOrEmpty(result))
            {
                return BadRequest(result);
            }

            var employeeCreated = await _employeeRepository.CreateAsync(employeeToCreate);

            return _mapper.Map<EmployeeResponse>(employeeCreated);
        }

        /// <summary>
        /// Изменение данных сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            var employeeToUpdate = await _employeeRepository.GetByIdAsync(id);
            if (employeeToUpdate == null)
            {
                return NotFound();
            }

            _mapper.Map(employeeRequest, employeeToUpdate);

            var result = await UpdateRoles(employeeRequest, employeeToUpdate);
            if (!string.IsNullOrEmpty(result))
            {
                return BadRequest(result);
            }

            var employeeUpdated = await _employeeRepository.UpdateAsync(employeeToUpdate);

            return _mapper.Map<EmployeeResponse>(employeeUpdated);
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (await _employeeRepository.DeleteAsync(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}