﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmployeeRequest, Employee>()
                .ForMember(x => x.Roles, opt => opt.Ignore());
            CreateMap<Employee, EmployeeResponse>();

            CreateMap<Role, RoleItemResponse>();
        }
    }
}