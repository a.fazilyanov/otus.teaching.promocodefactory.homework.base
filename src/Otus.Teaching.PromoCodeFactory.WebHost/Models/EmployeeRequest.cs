﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using AutoMapper;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "FirstName is required")]
        [MaxLength(64, ErrorMessage = "Max len 64")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [MaxLength(64, ErrorMessage = "Max len 64")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "E-mail is required")]
        [EmailAddress(ErrorMessage = "Is not a valid e-mail address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Roles is required")]
        [MinLength(1, ErrorMessage = "Roles is required")]
        public List<string> Roles { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be greater than or equal to 0")]
        public int AppliedPromocodesCount { get; set; }
    }
}